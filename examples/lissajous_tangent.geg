# geg data file
# all options can be changed within geg

toolbar = pictures_and_text
tooltips = on
print_as = pdf

xmin = -1.500000
xmax = 1.500000
ymin = -1.200000
ymax = 1.200000
tmin = 0.000000
tmax = 6.283185
amin = 0.000000
amax = 6.283185

graph_width = 1024
graph_height = 768
axes_lw = 0.4
grid_lw = 0.3
box_lw = 0.4
curves_lw = 0.5
point_radius = 1
coordinates_type = decimal
mode_type = parametric
zoom_xy = 3

zoom_factor = 0.100000
notch_spacing = 45
minimum_resolution = 0.000100
maximum_resolution = 10000.000000
interpolation_factor = 4
maximum_formulas = 500

do_Ox = 1
do_Oy = 1
do_xval = 1
do_yval = 1
do_color = 1
do_box = 0

# positions: 0=top left, 1=top right, 2=bottom left, 3=bottom right 
text_position = 2
formula_size = 20
number_size = 12

utility_editor = emacs
utility_eps_viewer = evince
utility_pdf_viewer = evince
utility_eps_to_pdf = ps2pdf -dEPSCrop -dPDFSETTINGS=/prepress
utility_svg_viewer = inkscape
utility_eps_to_svg = eps2svg

color[0] = { 1.000000, 1.000000, 1.000000 }
color[1] = { 0.500000, 0.600000, 0.700000 }
color[2] = { 0.800000, 0.700000, 0.600000 }
color[3] = { 0.800000, 0.800000, 0.800000 }
color[4] = { 0.200000, 0.800000, 0.400000 }
color[5] = { 0.500000, 0.500000, 0.500000 }
color[6] = { 0.000000, 0.000000, 0.000000 }
color[7] = { 0.800000, 0.000000, 0.000000 }
color[8] = { 0.000000, 0.000000, 0.800000 }
color[9] = { 0.000000, 0.800000, 0.000000 }
color[10] = { 0.800000, 0.800000, 0.000000 }
color[11] = { 0.800000, 0.000000, 0.800000 }
color[12] = { 0.000000, 0.800000, 0.800000 }
color[13] = { 0.400000, 0.400000, 0.400000 }
color[14] = { 0.000000, 0.000000, 0.000000 }

end_parameters

# Formulas
# use parametric mode
&param

# define Lissajous type functions
&fa(p)=cos(3*p)
&fb(p)=sin(4*p)

# draw the curve x=fa(t),y=fb(t) in the range [0;2*pi]
&tmin=0
&tmax=2*pi
x=fa(t);y=fb(t)

# introduce the tangent function to (fa,fb)
# explicitly  fu(t) = fa(c) + fa'(c) * (t-c) 
&fu=tgt_c_fa
&fv=tgt_c_fb

# draw the point (fa(0.3*pi),fb(0.3*pi))  (circle as parametric curve !)
&r=0.01
&c=0.3*pi
x=fa(c)+r*cos(t);y=fb(c)+r*sin(t)

# draw the tangent at c=0.3*pi
&param
&c=0.3*pi
x=fu(t);y=fv(t)
