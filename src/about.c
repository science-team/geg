/* about.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "about.h"
#include "i18n.h"

static GtkWidget *wi = NULL;

static void
ok_event(GtkWidget *widget, gpointer data)
{
  gtk_widget_destroy(wi);
  wi = NULL;
}

static gint
delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  gtk_widget_destroy(wi);
  wi = NULL;
  return(TRUE);
}

void 
about_event(GtkWidget *widget, gpointer data)
{
  GtkWidget *la, *bu;

  if(wi && wi->window) {
    gtk_widget_map(wi);
    gdk_window_raise(wi->window);
    return;
  }

  wi = gtk_dialog_new();

  gtk_signal_connect(GTK_OBJECT(wi), "delete_event",
                     GTK_SIGNAL_FUNC(delete_event), NULL); 

  gtk_window_set_wmclass(GTK_WINDOW(wi), _("about"), "Geg");
  gtk_window_set_title(GTK_WINDOW(wi), _("About geg"));
  gtk_window_set_policy(GTK_WINDOW(wi), FALSE, FALSE, FALSE);
  gtk_window_position(GTK_WINDOW(wi), GTK_WIN_POS_CENTER);

  gtk_container_border_width(GTK_CONTAINER(GTK_DIALOG(wi)->vbox), 5);

  /* ok button */
  bu = gtk_button_new_with_label(_("Ok"));
  gtk_signal_connect(GTK_OBJECT(bu), "clicked",
                     GTK_SIGNAL_FUNC(ok_event), NULL);

  GTK_WIDGET_SET_FLAGS(bu, GTK_CAN_DEFAULT);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(wi)->action_area), bu, TRUE, TRUE, 0);
  gtk_widget_grab_default(bu);
  gtk_widget_show(bu);

  /* about label */
  la = gtk_label_new("geg\n"
                     "A GTK+ Equation Grapher\n"
#ifdef VERSION
		     "Version "VERSION"\n"
#endif /* VERSION */
		     "By David Bryant 1999\n"
		     "daveb@acres.com.au");

  gtk_misc_set_padding(GTK_MISC(la), 40, 40);

  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(wi)->vbox), la, TRUE, TRUE, 0);
  gtk_widget_show(la);

  gtk_widget_show(wi);
}

