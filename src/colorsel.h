#ifndef __COLORSEL_H__
#define __COLORSEL_H__

#include <gtk/gtk.h>

void colorsel_event(GtkWidget *preview, gpointer data);
void show_color_in_preview(GtkWidget *preview, gdouble values[]);

#endif /* __COLORSEL_H__ */
