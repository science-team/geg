/* misc.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "misc.h"
#include <stdio.h>
#include <math.h>
#include <glib.h>

/* locally global data */
static char buf[100];

/* ftoa
 */
gchar
*ftoa(double f)
{
  char buf2[100];
  sprintf(buf2, "%%0.%df", MAX(5 - MAX((gint)log10(fabs(f)), 0), 0));
  sprintf(buf, buf2, f);

  return(buf);
}
  
/* ltoa
 */
gchar
*ltoa(long l)
{
  sprintf(buf, "%li", l);

  return(buf);
}

/* sign
 */
gint sign(float f)
{
  if(f < 0)
    return -1;
  return 1;
}
