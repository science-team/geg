#ifndef __COLORS_H__
#define __COLORS_H__

#include <gtk/gtk.h>

typedef struct {
  GdkColor back;
  GdkColor numb;
  GdkColor axes;
  GdkColor grid;  
  GdkColor sel;
  GdkColor gray;
} misc_colors;

extern guint n_f_colors;
extern gdouble *f_values;
extern GdkColor *f_colors;
extern misc_colors m_colors;

extern void alloc_func_colors(GdkColormap *colormap);
extern void alloc_color(GdkColor *color, gdouble *vals, GdkColormap *colormap);
extern void color_print(FILE *fd, int n);

#endif /* __COLORS_H__ */
