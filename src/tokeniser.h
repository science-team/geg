#ifndef __TOKEN_H__
#define __TOKEN_H__

#include <glib.h>

/* token types */
#define NUM     0x01	/* this is meant for constants, e.g. pi */
#define EQU     0x02    /* = */
#define ADD     0x03    /* + */
#define SUB     0x04    /* - */
#define MUL     0x05    /* * */
#define DIV     0x06    /* / */
#define LB      0x07    /* ( */
#define RB      0x08    /* ) */
#define VAR     0x09    /* variables a-z and other parameters */
#define POW     0x0a    /* ^ */
#define FUN     0x0b    /* functions, predefined or user defined */
#define LSB	0x0c    /* [ */
#define RSB	0x0d    /* ] */
#define LCB	0x0e    /* { */
#define RCB	0x0f    /* } */
#define SEP     0x10    /* ; */
#define INT     0x11    /* int */
#define TGT     0x12    /* tgt */
#define DER     0x13    /* ' */
#define RES     0x14    /* _ */
#define DEF     0x15    /* & */
#define COM     0x16    /* # */ /* comments */
#define ASK     0x17    /* ? */
#define SOL     0x18    /* = as initial character */
#define EOE     0x80    /* end of entry */
#define ERR     0xff    /* error */


typedef double (*Func) (double);

struct _token_info {
  gint type;
  double value;
  Func func;
  struct _token_info *next;
};

typedef struct _token_info token_list;
typedef struct _token_info token_node;

extern token_list *make_token_list(gchar *equation);
extern void free_list(token_list *list);
extern void set_output(gdouble x);

#endif /* __TOKEN_H__ */
