#ifndef __MISC_H__
#define __MISC_H__

#include <gtk/gtk.h>

gchar *ftoa(double f);

gchar *ltoa(long l);

gint sign(float f);

#endif /* __MISC_H__ */
