#ifndef __HELP_H__
#define __HELP_H__

#include <gtk/gtk.h>

void print_help();
void help_event(GtkWidget *widget, gpointer data);

#endif /* __HELP_H__ */
