// internationalisation
#ifndef I18N_H
#define I18N_H

#include <libintl.h>
#define  _(x)  gettext (x)

#endif
