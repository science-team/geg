#ifndef __LOCALFUNCTIONS_H__
#define __LOCALFUNCTIONS_H__

extern double l_sinc(double x);
extern double l_floor(double x);
extern double l_char(double x);
extern double l_clip(double x);
extern double l_sign(double x);
extern double l_cut(double x);
extern double l_out(double x);

extern double l_acsc(double x);
extern double l_asec(double x);
extern double l_acot(double x);
extern double l_csc(double x);
extern double l_sec(double x);
extern double l_cot(double x);
extern double l_phi(double x);

#endif /* __LOCAL_FUNCTIONS_H__ */
