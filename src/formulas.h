#ifndef __FORMULAS_H__
#define __FORMULAS_H__

#include <gtk/gtk.h>

typedef gint (*FormulaFunc) (gchar *formula, gint number);
typedef gint (*FormulaAllFunc) (gchar **formulas, gint nformulas,
				gdouble x1, gdouble x2,
				gdouble y1, gdouble y2);

extern void erase_event(GtkWidget *widget, gpointer data);
extern void remove_all_event(GtkWidget *widget, gpointer data);
extern void formula_foreach(FormulaFunc func);
extern void formula_forall(FormulaAllFunc func, 
		    gdouble x1, gdouble x2,
		    gdouble y1, gdouble y2);
extern gint formula_add(FormulaFunc func, gchar *formula);
extern gint formula_find(gchar *formula);
extern gchar** formula_list(void);
extern gint formula_count(void);

#endif /* __FORMULAS_H__ */
