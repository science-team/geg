/* localfunctions.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "formulas.h"
#include "localfunctions.h"
#include <math.h>
#include <string.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif /* M_PI */

extern gdouble output;

double
l_sinc(double x)
{
  if(x == 0)
    return 1;
  else
    return(sin(M_PI * x) / (M_PI * x));
}

double
l_floor(double x)
{
  if (x < 0)
    return(-(int)(-x-0.00000000000001)-1);
  else
    return((int)x);
}

double
l_char(double x)
{
  return(x >= 0);
}

double
l_sign(double x)
{
  if (x > 0) return 1;
  if (x < 0) return -1;
  return 0;
}

double
l_clip(double x)
{
  if (x >= 0)
    return 1;
  else
    return(NAN);
}

double
l_cut(double x)
{
  if (x > 0)
    return 1;
  else
    return(NAN);
}

double
l_acsc(double x)
{
  if (x == 0) return(NAN);
  return(asin(1/x));
}

double
l_asec(double x)
{
  if (x == 0) return(NAN);  
  return(acos(1/x));
}

double
l_acot(double x)
{
  if (x == 0) return(NAN);  
  return(atan(1/x));
}

double
l_csc(double x)
{
  if (x == 0) return(NAN);  
  return(1/sin(x));
}

double
l_sec(double x)
{
  gdouble y = cos(x);
  if (y == 0) return(NAN);  
  return(1/y);
}

double
l_cot(double x)
{
  if (x == 0) return(NAN);  
  return(1/tan(x));
}

double
l_phi(double x)
{
  return(0.5 * erfc(-x * M_SQRT1_2));
}
