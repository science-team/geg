#ifndef __LOG_H__
#define __LOG_H__

#include <gtk/gtk.h>

void create_log(GtkWidget *widget);
void write_log(gchar *message);
void addto_log(gchar *message);
void clear_log(GtkWidget *widget, gpointer data);

#endif /* __LOG_H__ */
