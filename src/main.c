/* main.c
 * geg, a GTK+ Equation Grapher
 * David Bryant 1998
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include "app.h"
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include "i18n.h"

extern void initialize_prefs();

int
main(int argc, char *argv[])
{
  initialize_prefs();
  parse_command_line(argc, argv);
  parse_input_file();

  bindtextdomain ("geg", "/usr/share/locale");
  textdomain ("geg");
  bind_textdomain_codeset ("geg", "UTF-8");

  gtk_set_locale();

  gtk_init(&argc, &argv);

  // important : the rcfile must be with a standard LC_NUMERIC locale  
  setlocale (LC_NUMERIC, "C");
  
  if(app())
    g_error(_("error initialising"));

  gtk_main();

  return(0);
}
