geg (2.0.9-5) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 19 Oct 2022 05:11:16 -0000

geg (2.0.9-4) unstable; urgency=medium

  * fixed watch file

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 27 Mar 2021 15:08:11 +0100

geg (2.0.9-3) unstable; urgency=medium

  * Updated Vcs locations to use Salsa's addresses
  * Updated Standards-Version to 4.3.0, dh level to 11

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 06 Jan 2019 16:04:33 +0100

geg (2.0.9-2) unstable; urgency=medium

  * upgraded Standards-Version: 4.1.1, debhelper: 10

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 19 Nov 2017 19:50:25 +0100

geg (2.0.9-1) UNPUBLISHED; urgency=medium

  * new upstream release

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 30 Jun 2016 18:03:14 +0200

geg (2.0.8-1) unstable; urgency=medium

  * Imported Upstream version 2.0.8. Closes: #824058

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 18 May 2016 14:02:58 +0200

geg (2.0.6-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Add Vcs-Fields and Homepage
  * Debian Science maintainers list as Maintainer
  * cme fix dpkg-control


 -- Georges Khaznadar <georgesk@debian.org>  Wed, 18 May 2016 13:57:25 +0200

geg (2.0.6-1) unstable; urgency=medium

  * New upstream release
  * Second upload since the RoM in year 2009. Closes: #819392

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 08 May 2016 20:00:33 +0200

geg (2.0.2-1) unstable; urgency=medium

  * updated to the new version published by Jean-Pierre Demailly
  * upgraded Standards-Version to 3.9.7
  * First upload since the RoM in year 2009. Closes: #819392

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 28 Mar 2016 00:37:43 +0200

geg (2.0~beta-1) UNPUBLISHED; urgency=low

  [ Barry deFreese ]
  * New version 2.0
  * Print out can be either PDF or SVG
  * Parametric mode added: t-->(x,y)
  * Formula parser enhanced: variables a-z can now be used as parameters

  [ Georges Khaznadar ]
  * defined myself as maintainer
  * upgraded Standards-Version to 3.9.6, compat to 9
  * changed debian/rules to use new dh features; used the option
    --with autoreconf
  * added a build-dependency on dh-autoreconf
  * defined the new source format 3.0 with quilt
  * added debian/graph_icon.png to debian/source/include-binaries
  * removed the leading article from description synopsis
  * renamed configure.in to configure.ac
  * replaced INCLUDES by AM_CPPFLAGS in src/Makefile.am
  * repacked the upstream package and sent it back to the author

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 23 Mar 2016 10:26:57 +0100

geg (1.0.3-1) unstable; urgency=low

  * Added Edit and Print functions

 -- Barry deFreese <bdefreese@debian.org>  Fri, 18 Mar 2016 13:28:45 -0500

geg (1.0.2-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build with GTK2. (Closes: #515284).
  * Remove deprecated Encoding field from desktop file.
  * Add ${misc:Depends} for debhelper package.
  * Make clean not ignore errors.
  * Add appropriate copyright holder to debian/copyright.
    + Version GPL path to GPL-2.
  * Bump debhelper build-dep to 5.
    + Move DH_COMPAT to debian/compat and set to 5.
  * Bump Standards Version to 3.8.0.
    + Menu policy transition.

 -- Barry deFreese <bdefreese@debian.org>  Tue, 24 Feb 2009 13:28:45 -0500

geg (1.0.2-6) unstable; urgency=low

  * Acknowledge NMU. (closes: #346694)
  * Added a desktop file. (closes: #348311)
  * Added French translation, contributed by Georges Khaznadar.

 -- Frederic Peters <fpeters@debian.org>  Thu, 23 Feb 2006 13:03:33 +0100

geg (1.0.2-5.1) unstable; urgency=low

  * Non-maintainer upload.
  * Update build-deps for xlibs-dev removal; closes: #346694.
  * Credit and Big Thanks to Justin Pryzby
    <justinpryzby@users.sourceforge.net> for the patch and testing.

 -- Amaya Rodrigo Sastre <amaya@debian.org>  Thu, 19 Jan 2006 12:23:31 +0100

geg (1.0.2-5) unstable; urgency=low

  * debian/control: updated to current policy
    * means no more /usr/doc/ symlink, removed automatically by the grace
      of a new debhelper (closes: #322831)
  * debian/menu: quote strings (fixes lintian warnings)
  * debian/rules: debhelper compatibility: 3 (and updated build-depends
    accordingly)
  * debian/rules: use dh_installman, no more dh_installmanpages

 -- Frederic Peters <fpeters@debian.org>  Sat, 13 Aug 2005 17:43:35 +0200

geg (1.0.2-4) unstable; urgency=low

  * debian/control: Updated Build-Dependencies (xlib6g-dev -> xlibs-dev)
    (closes: #170171)
  * debian/changelog: removed emacs modelines

 -- Frederic Peters <fpeters@debian.org>  Fri, 22 Nov 2002 09:05:21 +0100

geg (1.0.2-3) unstable; urgency=low

  * Rebuilt against XFree86 4.0 libs

 -- Frederic Peters <fpeters@debian.org>  Fri, 10 Nov 2000 18:48:56 +0100

geg (1.0.2-2) unstable; urgency=low

  * Source only upload so it will be built with X 3.3.6 (closes: #74991)

 -- Frederic Peters <fpeters@debian.org>  Sun, 29 Oct 2000 23:49:47 +0100

geg (1.0.2-1) unstable; urgency=low

  * New upstream release. (closes: #74364)
  * debian/control: Added debhelper to Build-Depends line

 -- Frederic Peters <fpeters@debian.org>  Mon, 16 Oct 2000 22:28:58 +0200

geg (1.0.1-0.1) unstable; urgency=low

  * Debian QA upload as requested by Frederic on debian-devel.
  * New upstream release (closes: #50799).
  * src/parser.c (term): Handle precedences correctly. All other functions
    updated accordingly. (closes: #50625).
  * debian/copyright: Updated location of GPL license
  * debian/copyright: Updated download location
  * debian/control: Changed standards-version to 3.1.0, added Build-Depends.
  * Removed debian/README.debian because the content no longer applies.
  * debian/geg.1: Wrote basic manpage.
  * debian/rules: Adjusted installation accordingly.
  * src/help.c: Changed help string to reflect change in equation parser.

 -- Torsten Landschoff <torsten@debian.org>  Tue, 23 Nov 1999 16:25:46 +0100

geg (0.99.0-3) unstable; urgency=low

  * Compiled against libgtk1.0 since the 'Drawing Area' widget no longer works
    like before (since ????). (Fixes#46099)

 -- Frederic Peters <fpeters@debian.org>  Mon,  8 Mar 1999 22:27:49 +0100

geg (0.99.0-2) unstable; urgency=low

  * Compiled against libgtk1.2

 -- Frederic Peters <fpeters@debian.org>  Mon,  8 Mar 1999 22:27:49 +0100

geg (0.99.0-1) unstable; urgency=low

  * New upstream release.
  * Now linked against libgtk1.1.15, feel free to send request to go back
    to 1.0 :)

 -- Frederic Peters <fpeters@debian.org>  Mon, 15 Feb 1999 19:05:26 +0100

geg (0.15.0-1) unstable; urgency=low

  * New upstream release.

 -- Frederic Peters <fpeters@debian.org>  Mon,  5 Oct 1998 21:44:05 +0200

geg (0.12-1) unstable; urgency=low

  * Initial Release.

 -- Frederic Peters <fpeters@debian.org>  Sun, 13 Sep 1998 23:05:47 +0200
